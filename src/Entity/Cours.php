<?php

namespace App\Entity;

use App\Repository\CoursRepository;
use App\Repository\NoteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: CoursRepository::class)]
#[Vich\Uploadable]
#[UniqueEntity('title', 'Le titre est déja utilisé. Merci de changer.')]
#[UniqueEntity('content', 'Mais t\'es grave toi ! Deux fois le même contenu c\'est louche... Recommence !!!!')]
class Cours
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(length: 50)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 50)]
    private ?string $title = null;
    #[Vich\UploadableField(mapping: 'products', fileNameProperty: 'imageName')]
    private ?File $imageFile = null;

    #[ORM\Column(type: 'string', nullable:true)]
    private ?string $imageName = null;

    #[ORM\Column(length: 10000)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 50, max: 10000)]
    private ?string $content = null;

    #[ORM\Column(length: 50)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 50)]
    private ?string $author = null;

    #[ORM\Column]
    #[Assert\NotBlank()]    
    private ?\DateTimeImmutable $createdAt = null;

    #[Assert\NotBlank()]
    #[ORM\Column(type: 'datetime_immutable', nullable:true)]  
    private ?\DateTimeInterface $updatedAt = null;
    public function __construct()
{
    $this->createdAt =new \DateTimeImmutable();    
    $this->notes = new ArrayCollection();
}
    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $UpdateAt = null;
    #[ORM\ManyToOne(inversedBy: 'cours')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column]
    private ?bool $isPublic = false;

    #[ORM\OneToMany(mappedBy: 'cours', targetEntity: Note::class, orphanRemoval: true)]
    private Collection $notes;
    private ?float $moyenne = null;   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
           
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }
    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->UpdateAt;
    }

    public function setUpdateAt(\DateTimeImmutable $UpdateAt): self
    {
        $this->UpdateAt = $UpdateAt;

        return $this;
    }
    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * @return Collection<int, Note>
     */
    public function getNote(): Collection
    {
        return $this->notes;
    }
    
   

  
    public function getMoyenne()
    {
        $notes = $this->notes;

        if ($notes->toArray() === []) {
            $this->moyenne = null;
            return $this->moyenne;
        }

        $total = 0;
        foreach ($notes as $note) {
            $total += $note->getNote();
        }

        $this->moyenne = $total / count($notes);

        return $this->moyenne;
    }

    /**
     * @return Collection<int, Note>
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes->add($note);
            $note->setCours($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getCours() === $this) {
                $note->setCours(null);
            }
        }

        return $this;
    }

}
