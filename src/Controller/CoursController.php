<?php

namespace App\Controller;

use App\Entity\Cours;
use App\Entity\Mark;
use App\Entity\Note;
use App\Form\CoursType;
use App\Form\MarkType;
use App\Form\NoteType;
use App\Repository\CoursRepository;
use App\Repository\MarkRepository;
use App\Repository\NoteRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\DomCrawler\Form;
use Symfony\Component\HttpFoundation\Request;

class CoursController extends AbstractController
{   
    // Designation de la route
    #[Route('/cours', name: 'app_cours', methods: ['GET'])]
    // autoriser que les utilisateurs inscris
    #[IsGranted('ROLE_USER')]
    // Affichage de tous les Cours dans un pableau sur la page Cours
    public function index(
       CoursRepository $repository,
        // permets la pagination
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $cours = $paginator->paginate(
            // récupère toutes les données de la bdd
            $repository->findBy(['user' => $this->getUser()]),
            // on les classe 5 par page
            $request->query->getInt('page', 1),
            5,
        );
        return $this->render('pages/cours/index.html.twig', [
            'cours' => $cours
        ]);
    }
    #[Route('/cours/public',  name: 'cours_public', methods: ['GET'])]
    public function coursPublic(
        CoursRepository $repository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $cours = $paginator->paginate(
            $repository->findPublicCours(null),
            // on les classe 5 par page
            $request->query->getInt('page', 1),
            4,
        );
        return $this->render('pages/cours/coursPublic.html.twig', [
            'cours' => $cours
        ]);
    }
    

    // route qui sera marqué dans la barre du navigateur
    #[Route('/cours/nouveau', 'cours_new', methods: ['GET', 'POST'])]
    // création d'un nouvel Cours et envoie en bdd
    public function new(Request $request, EntityManagerInterface $manager): Response
    {
        $cours = new Cours();
        $form = $this->createForm(CoursType::class, $cours);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cours = $form->getData();
            $cours->setUser($this->getUser());
            // persist = on met dans la boite
            $manager->persist($cours);
            //    flush on envoie vers la bdd
            $manager->flush();
            // rajout d'une alerte pour dire que ca a fonctionné
            $this->addFlash(
                'danger',
                'C\'est bon gars !!! c\'est dans la boite !!!! '
            );
            return $this->redirectToRoute('app_cours');
        }
        return $this->render('pages/cours/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
    // création d'une fonction pour modifier un Cours
    // Seul le redacteur de l'Cours peut le modifier
    #[Security("is_granted('ROLE_USER') and user === cours.getUser()")]
    // route qui sera marqué dans la barre du navigateur
    #[Route('/cours/edition/{id}', 'cours.edit', methods: ['GET', 'POST'])]

    // Pas la peine de chercher par l'id avec symfony car il le fait en automatique
    //  public function edit(CoursRepository $repository, int $id): Response
    public function edit(Cours $cours, Request $request, EntityManagerInterface $manager): Response
    {
        // grâce a symfony pas la peine de chercher par l'id car comme l'Cours contient en bdd un id symfony le cherche automatiquement
        // $Cours = $repository->findOneBy(['id' => $id]);
        $form = $this->createForm(CoursType::class, $cours);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cours = $form->getData();;
            // persist = on met dans la boite
            $manager->persist($cours);
            //    flush on envoie vers la bdd
            $manager->flush();
            // rajout d'une alerte pour dire que ca a fonctionné
            $this->addFlash(
                'danger',
                'Ca y est tu as corrigé tes conneries ? '
            );
            return $this->redirectToRoute('app_cours');
        }
        return $this->render('pages/cours/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    // Supprimer un Cours
    // route du navigateur
    // Seul le redacteur de l'Cours peut le supprimer
    #[Security("is_granted('ROLE_USER') and user === cours.getUser()")]
    #[route('/cours/suppression/{id}', 'cours.delete', methods: ['GET'])]
    public function delete(EntityManagerInterface $manager, Cours $cours): Response
    {
        $manager->remove($cours);
        $manager->flush();
        $this->addFlash(
            'danger',
            'Hop poubelle ! Bon débarras!! '

        );


        return $this->redirectToRoute('app_cours');
    }


    #[Security("is_granted('ROLE_USER') and (cours.getIsPublic() === true || user === cours.getUser())")]
    #[Route('/cours/{id}', 'cours.show', methods: ['GET', 'POST'])]
     public function show(Cours $cours, Request $request,  NoteRepository $noteRepository,
     EntityManagerInterface $manager
): Response
// Mise en place du systeme de notation . avec une nouvelle note
     { $note = new Note();
        $form = $this-> createForm(NoteType::class, $note);
        $form ->handleRequest($request);
        //  si le formulaire est submit et valid
        if($form-> isSubmitted() && $form ->isValid() ){
            
            // attacher à un utilisateur
            $note->setUser($this->getUser())
            ->setCours($cours);
//  Pour verifier que l'utilisateur n'a pas encore noté l'Cours
        $existingNote = $noteRepository->findOneBy([
            'user' => $this->getUser(),
            'cours' => $cours
        ]);

        if (!$existingNote) {
            $manager->persist($note);
        } else {
            // changer la note deja donnée si le user a deja noter l Cours
            $existingNote->setNote(
                $form->getData()->getNote()
            );
        }

        $manager->flush();
// valider et afficher une alerte pour confirmer la note
        $this->addFlash(
            'success',
            'Votre note a bien été prise en compte.'
        );

        return $this->redirectToRoute('cours.show', ['id' => $cours->getId()]);
    }        
         return $this->render('pages/cours/show.html.twig', [
             'cours' => $cours,
             'form' => $form -> createView()
         ]);
     }
}



